package com.example.applistproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.applistproject.databinding.ActivityMainBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "testing";
    //utk linkkan antara main activity dgn adapter
    private ActivityMainBinding binding;
    private RecyclerAdapter recyclerAdapter = new RecyclerAdapter();
//    private Button btnregister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        //utk connect dgn database firestore
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        List<Details> customerDetails = new ArrayList<>();

        CollectionReference collection = db.collection("CustomerDetail");
//
//        Map<String, Object> details = new HashMap<>();
//        details.put("name", "Muhmmad Ridha");
//        details.put("age", 25;
//        details.put("Id", 2017100469);
//        details.put("Phone Number", 0142674602);

//        btnregister = binding.btnRegister;
//
//        btnregister.setOnClickListener(new View.OnClickLi   stener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent = new Intent(MainActivity.this, NextPage.class);
//                startActivity(intent);
//
//            }
//        });


        //utk baca di database firecloud
        db.collection("CustomerDetail")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Details details = document.toObject(Details.class);
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                customerDetails.add(details);//  masukkan data dalam array

                            }
                            recyclerAdapter.setCustomerDetails(customerDetails);//pass data dalam adapter
                            recyclerAdapter.notifyDataSetChanged();//memang wajib ada utk notify data berubah
                            Log.d(TAG, "onComplete: end loop");

                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });


        //ni utk panggil UI dri activity_xml
        RecyclerView rv = binding.rv;
        rv.setLayoutManager((new LinearLayoutManager(this)));
        rv.setAdapter(recyclerAdapter);

//        setContentView(R.layout.activity_main);
    }
}
