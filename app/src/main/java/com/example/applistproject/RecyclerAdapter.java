package com.example.applistproject;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.applistproject.databinding.CustomerDetailBinding;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private List<Details> customerDetails = new ArrayList<>(); //Declare sahaja

    @NonNull
    @Override

    // step first amik je 3 code ni yg lain cuma ItemPeopleBinding
    //dah buat ni pergi ke mainactivity utk link kan dia
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
         CustomerDetailBinding binding = CustomerDetailBinding.inflate(layoutInflater, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(customerDetails.get(holder.getAdapterPosition()));

    }

    @Override
    public int getItemCount() {
        return  customerDetails.size();// utk keluar kan semua dlm data base
//        return 10;
    }

    public void setCustomerDetails(List<Details> customerDetails) {
        this.customerDetails = customerDetails;
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        private final CustomerDetailBinding binding;//Declare atas ni dulu lepas tu assign kan kat bawah dia

        public ViewHolder(@NonNull CustomerDetailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;//assignkan lepas dh declare

        }

        public void bind(Details details) {
            // yg ni utk keluar kan semua apa yg ada kat data base
        binding.name.setText("name"+details.getName());
        binding.age.setText("age"+details.getAge());
        binding.id.setText("id"+details.getId());
        binding.id.setText("phone number"+details.getPhoneNumber());

        }
    }
}
